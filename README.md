Run instructions:

1. Install NPM on the machine the tests run on

2. CMD cd to  *\e2e-lp\LeasePlanAssesment
3. Run this command in LeasePlanAssesment folder: npm install
4. Run this command in LeasePlanAssesment folder: npm run cypress:run  (to run tests headless)
5. Run this command in LeasePlanAssesment folder: npm run cypress:open (to view tests being executed in Cypress UI)