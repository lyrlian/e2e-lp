let baseUrl;


// I have seen many different E2E tags being used in website: data-e2e-paper, data-e2e-id, data-e2e-heading etc Why are so many different tags used? Uniformity would make it easier to code for.

describe('Test base functionality on Leaseplan Customer Service website',
    function () {    

        after(function () {
            // runs once after all tests in the block
            cy.clearCookies()
        });

        it('After approving cookies the customer service website loads as expected',
            function () {
                cy.visit("/nl-nl/klantenservice/");
                // "Manually" Accept cookies, can also be written into a more fancier "if element visible click accept button function"
                cy.get('[aria-label="Cookies accepteren"]').click();
                cy.get('[data-tag-id="component-cx-search"]').click();
                cy.get('[data-e2e-heading]').should('contain', "Hoe kunnen we je helpen?");

            });

        it('Search function works as expected and opens the requested Article',
            function () {
                //Wait for XHR response after typing in dynamic search window (this could be placed Fixtures folder to be re-used)
                cy.server();
                cy.route('**/api2/**').as('getData');
                cy.get('[data-e2e-text-input-input]').type("private lease");
                //Wait for results to load in grid
                cy.wait('@getData');
                //Confirm search results contain expected article, click to open article, could verify contents again in another "should"
                cy.get('[data-tag-id="results-question-Hoe werkt de kredietcheck bij Private Lease?"]').should('exist').click(); 
            });
    });